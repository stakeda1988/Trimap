import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var mapView: UIView!
    var googleMap : GMSMapView!
    var lm: CLLocationManager! = nil
    var longitude: CLLocationDegrees!
    var latitude: CLLocationDegrees!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        lm = CLLocationManager()
        lm.delegate = self
        lm.requestAlwaysAuthorization()
        lm.desiredAccuracy = kCLLocationAccuracyBest
        lm.distanceFilter = 1
        lm.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation){
        longitude = newLocation.coordinate.longitude
        latitude = newLocation.coordinate.latitude

        let zoom: Float = 20
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(latitude,longitude: longitude, zoom: zoom)
        googleMap = GMSMapView(frame: CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height))
        googleMap.camera = camera
        mapView.addSubview(googleMap)
        
        let marker: GMSMarker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.map = googleMap
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("Error")
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchBar.text) // これで検索バーの値が取れる
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        var geocoder = CLGeocoder()
        geocoder.geocodeAddressString("1 infinite loop, cupertino, ca") { (placemarks, error) -> Void in
            if let placemark = placemarks?[0] {
                print(placemark.location!.coordinate.latitude, placemark.location!.coordinate.longitude)
                let zoom: Float = 10
                let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(placemark.location!.coordinate.latitude,longitude: placemark.location!.coordinate.longitude, zoom: zoom)
                self.googleMap = GMSMapView(frame: CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height))
                self.googleMap.camera = camera
                self.mapView.addSubview(self.googleMap)
                
                let marker: GMSMarker = GMSMarker()
                marker.position = CLLocationCoordinate2DMake(placemark.location!.coordinate.latitude, placemark.location!.coordinate.longitude)
                marker.map = self.googleMap
                
            } else {
                // 検索リストに無ければ
                print("存在しません")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}